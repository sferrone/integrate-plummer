import numpy as np
import sys 
import os
import time 
from astropy import constants as const
from astropy import units as u 
import json 

def PlummerMassProfile(Mt,Rc,r):
    """
    the amount of enclosed mass as a function of r 
    Parameter:
    M: total mass of the Plummer sphere
    Rc: core radius of the Plummer sphere
    r: radius at which we want to know the enclosed mass
    """
    return Mt * r**3 * (r**2 + Rc**2 )**(-3/2)   

def PlummerRadius(Mx,Rc):
    """
    The radius at which we enclose Menc
    Parameter:
    Mx: the fraction of the total mass we want to enclose
    Rc: core radius of the Plummer sphere
    """
    return Rc*np.sqrt((Mx**(2/3))/(1-Mx**(2/3)))

def UniformSphere(NP):
    """
    Generate a uniform distribution of points on a sphere
    Parameter:
    NP: number of particles
    RETURNS
    phi: angle in longitude
    theta: angle in latitude
    thetaWRONG: incorrect latitude sampling
    """
    # uniform angle in longitude
    phi = np.random.rand(NP)*2*np.pi
    # inverse transform sampling for the angle in latitude
    theta = np.arccos(2*np.random.rand(NP)-1)
    # generate the incorrect latitude sampling for comparison
    thetaWRONG = np.pi*np.random.rand(NP) 
    return phi,theta,thetaWRONG

def PotentialEnergy(Mt,a,r,G=1):
    """
    Mt: total mass
    a = plummer scale length
    r: radius
    G = gravitational constant

    """
    return -G*Mt/np.sqrt(r**2 + a**2)

def Energy(Mt,a,r,v,G=1):
    """
    Mt: total mass
    a = plummer scale length
    r: radius
    v: velocity
    G = gravitational constant

    """
    return  (1/2)*v**2 + PotentialEnergy(Mt,a,r,G)

def DFPlummer(Mt,a,r,v,G=1):
    """
    The DF
    """
    E=Energy(Mt,a,r,v,G)
    return (-E)**(7/5)

def EscapeVelocity(Mt,a,r,G=1):
    """
    the escape velocity
    """
    return np.sqrt(-2*PotentialEnergy(Mt,a,r,G))

def velocityCDF(vesc,DistFunc,Mt,Rc,r,G=1,relNumGaurd=1000,nsamp=1000):
    """
    purpose: generate the CDF for the velocity distribution function
    INPUTS:
        vesc: escape velocity at the radius r
        DistFunc: the distribution function
        Mt: total mass of the system
        Rc: core radius of the Plummer sphere
        r: radius of the particle of interest
        G: gravitational constant
        relNumGaurd: The numerical gaurd for not including vesc in the grid
    OUTPUTS:
        CDFnorm: the normalized CDF
        vels: the velocity grid
    """
    dv = vesc/relNumGaurd
    vels=np.linspace(0,vesc-dv,nsamp)
    DF=DistFunc(Mt,Rc,r,vels,G)
    # remember, the velocity sphere, for equal probability
    CDF=np.cumsum(DF*(vels**2)*np.pi*4)
    CDFmax=CDF.max()
    CDFmin=CDF.min()
    CDFnorm=(CDF-CDFmin)/(CDFmax-CDFmin)
    return CDFnorm,vels
    
def velocitySampling(DistFunc,Mt,Rc,rAll,G=1,storeCDF=True):
    """ 
    Sample the velocities of a particle distribution
    INPUTS:
        DisFunc: the distribution function
        Mt: total mass of the system
        Rc: core radius of the Plummer sphere
        rAll: array of radii of the particles of interest
        G: gravitational constant
        storeCDF: boolean to store the CDF
    OUTPUTS:    
        vx,vy,vz: arrays of the sampled velocities
        OPTIONAL
            CDFAll: all cummulative distribution functions for each particle in rAll
            vels: all of the velocity grids for each particle in rAll
            rannums (np.array, same size as rAll): each random number corresponding to the random inverse transform sampling each CDF
    """
    # get the number of particles
    NP=len(rAll)
    # get the v_escape for each particle
    vesc=EscapeVelocity(Mt,Rc,rAll,G)
    # initialize velocity array
    speed=np.zeros(NP)
    # number of samples to take for the CDF
    nsamp=1000
    if storeCDF:
        testVels=np.zeros((NP,nsamp))
        CDFAll=np.zeros((NP,nsamp))
        # store the random numbers
        rannums=np.zeros(NP)
        # loop over the particles
        for i in range(NP):
            CDFnorm,vels=velocityCDF(vesc[i],DistFunc,Mt,Rc,rAll[i],G=G,relNumGaurd=1000,nsamp=1000)
            # get the random number
            rannum=np.random.rand(1)
            # inverse transform sample the velocity
            speed[i]=np.interp(rannum,CDFnorm,vels)
            # store the CDF and velocity grid
            CDFAll[i,:]=CDFnorm
            testVels[i,:]=vels
            # store the random number
            rannums[i]=rannum
    else:
        # don't store the extra information
        for i in range(NP):
            CDFnorm,vels=velocityCDF(vesc[i],DistFunc,Mt,Rc,rAll[i],G=G,relNumGaurd=1000,nsamp=1000)
            # get the random number
            rannum=np.random.rand(1)
            # inverse transform sample the velocity
            speed[i]=np.interp(rannum[0],CDFnorm,vels)
        
    # get the angles for the velocity
    vel=np.random.random((NP,3))-0.5
    vel=vel/np.sqrt(np.sum(vel**2,axis=1))[:,None]
    vel=vel*speed[:,None]
    vx,vy,vz=vel[:,0],vel[:,1],vel[:,2]

    if storeCDF:
        return vx,vy,vz,speed,CDFAll,testVels,rannums
    else:
        return vx,vy,vz,speed

    

def main(Mt,rc,NP=1000,G=1):
    '''
    purpose: generate initial conditions for a Plummer sphere
    input:  Mt = total mass of the sphere
            rc = core radius of the sphere
            NP = number of particles
            G = gravitational constant
    output: x,y,z,vx,vy,vz = initial conditions
    '''
    # random uniform sampling of the enclused mass
    Mxs=np.random.rand(NP)
    # inverse transform sampling for the radius
    r=PlummerRadius(Mxs,rc)
    phi,theta,_ = UniformSphere(NP)
    # generate x,y,z
    x = r*np.sin(theta)*np.cos(phi)
    y = r*np.sin(theta)*np.sin(phi)
    z = r*np.cos(theta)
    # from wikipedia
    vx,vy,vz,_=velocitySampling(DFPlummer,Mt,rc,r,G=G,storeCDF=False)
    return x,y,z,vx,vy,vz

def PlummerCircVel(Mt,Rc,r,G):
    """
    purpose: compute the circular velocity of a Plummer sphere
    THEY MUST COME WITH ASTROPY UNITS ! 
    INPUTS:
        Mt: total mass of the system
        Rc: core radius of the Plummer sphere
        r: radius of the particle of interest
        G: gravitational constant
    OUTPUTS:
        vc: the circular velocity
    """
    # compute the circular velocity 
    vc = r*np.sqrt(G*Mt/(r**2 + Rc**2)**(3/2))
    return vc.to(u.km/u.s)


if __name__ == "__main__":
    mass=float(sys.argv[1])
    Rc=float(sys.argv[2])
    NP=int(sys.argv[3])
    print("PLUMMER SPHERE")
    print("Mass", mass, "solar masses")
    print("Char radius", Rc, "pc")
    print("# particles", NP, )
    outunits=u.pc * (u.km / u.s)**2 / (u.Msun)
    # compute the gravitational constant
    G = const.G.to(outunits)
    vc=PlummerCircVel(mass*u.Msun,Rc*u.pc,Rc*u.pc,G)
    print("Circular velocity", vc)
    period = (2*np.pi*Rc*u.pc)/vc
    print("Period", period)
    dt = period/100000
    # compute the circular velocity 
    # store the parameters
    params = {"mass":mass,"Rc":Rc,
              "NP":NP,"G":G.value,"dt":dt.value, "period":period.value}
    print(params)
    # save the parameters as a json file
    outfname="../initconds/params-isotropic-Plummer-{:d}-particles.json".format(NP)
    with open(outfname, 'w') as fp:
        json.dump(params, fp)

    
    st = time.time()
    x,y,z,vx,vy,vz=main(Mt=mass,rc=Rc,NP=NP,G=G.value)
    print("time to generate initial conditions of {:d} particles: \n".format(NP),time.time()-st, "seconds")
    # make sure the output directory exists
    os.makedirs("../initconds/",exist_ok=True)
    # save the initial conditions
    fname = "../initconds/initCond-isotropic-Plummer-{:d}-particles.txt".format(NP)
    np.savetxt(fname.format(NP),np.array([x,y,z,vx,vy,vz]).T)
    print("OUT POSITIONS :",fname)
    print("OUT PARAMS", outfname )

    
