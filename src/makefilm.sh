# use ffmpeg to make a film from a set of images in a directory
mkdir ../videos
ffmpeg -framerate 20 -i $1/frame%d.png -c:v libx264 -pix_fmt yuv420p ../videos/$2.mp4
