MODULE accelerations



contains

SUBROUTINE Plummer(xP,yP,zP,axP,ayP,azP,phiP,M,Rc,NP)
    ! Compute the force and potential of a Plummer sphere on massless particles
    ! xP,yP,zP: position
    ! axP,ayP,azP: acceleration on the particle
    ! phiP: potential on the particle
    ! M: total mass of plummer system
    ! Rc: core radius of plummer system
    ! Np: number of particles

    ! NOTE
    ! With f2py, you do no need to pass the size of the arrays as an argument when calling the function from python

    IMPLICIT NONE
    
    ! Input variables
    INTEGER, INTENT(IN) :: NP
    REAL(8), INTENT(IN),DIMENSION(NP) :: xP, yP, zP
    REAL(8), INTENT(IN) :: M, Rc
    ! Output variables
    REAL(8), INTENT(OUT),DIMENSION(NP) :: axP, ayP, azP, phiP
    
    ! internal variables 
    INTEGER :: i
    REAL(8) :: rgc

    ! Compute the acceleration and potential
    DO i=1,NP
        rgc = SQRT(xP(i)**2 + yP(i)**2 + zP(i)**2)
        phiP(i) = -M/(rgc + Rc)
        axP(i) = -M*xP(i)/(rgc + Rc)**2
        ayP(i) = -M*yP(i)/(rgc + Rc)**2
        azP(i) = -M*zP(i)/(rgc + Rc)**2
    END DO

    end SUBROUTINE Plummer
    




END MODULE accelerations