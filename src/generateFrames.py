import numpy as np 
import matplotlib.pyplot as plt
import os 
import sys 

def main(directory,fnameInitConds):
    # set dark style
    plt.style.use("dark_background")
    # list all the files in the output directory
    files = os.listdir(directory)
    # only keep the files that start with "frame"
    files = [f for f in files if f.startswith("frame")]
    # drop every other file
    # files = files[::2]
    # sort the files by the frame number
    framenumber = [int(f.split("-")[1].split(".")[0]) for f in files]
    # print(framenumber)
    sortdexes=np.argsort(framenumber)
    framenumber=np.array(framenumber)[sortdexes]
    files=np.array(files)[sortdexes]
    # get the initial conditions file
    dataIC = np.loadtxt(fnameInitConds)
    xIC,yIC,zIC = dataIC[:,0], dataIC[:,1], dataIC[:,2]

    # make an output directory for the images
    outdir = "../outputIMAGES/"
    os.makedirs(outdir, exist_ok=True)
    outdir=outdir+"isotropic-plummer-{:d}/".format(len(xIC))
    os.makedirs(outdir, exist_ok=True)

    # get max for all the limits
    maxval = np.max(np.abs([xIC,yIC,zIC]))
    factor=1/10
    # make the plot
    fig, ax = threeDplot(xIC,yIC,zIC)
    # set the limits
    ax.set_xlim(-factor*maxval,factor*maxval)
    ax.set_ylim(-factor*maxval,factor*maxval)
    ax.set_zlim(-factor*maxval,factor*maxval)    
    # save the plot
    outname="frame"+"0"+".png"
    fig.savefig(outdir+outname, dpi=300)
    # loop over the files
    for i in range(len(files)):
        fname = files[i]
        print(i, fname)
        # load the data
        data = np.loadtxt(directory+fname)
        x,y,z = data[:,0], data[:,1], data[:,2]
        # make the plot
        fig, ax = threeDplot(x,y,z)
        # set the limits
        ax.set_xlim(-factor*maxval,factor*maxval)
        ax.set_ylim(-factor*maxval,factor*maxval)
        ax.set_zlim(-factor*maxval,factor*maxval)    

        # save the plot
        # extract the frame number from the file name formatted as frame-000000.txt
    


        outname="frame"+str(i+1)+".png"
        fig.savefig(outdir+outname, dpi=300)
        # close the plot
        plt.close(fig)


def threeDplot(x,y,z):
    # make the plot in 3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x,y,z, s=1, c='w', marker='o',alpha=0.5)
    # remove all labels
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_zticklabels([])
    # remove all ticks
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    # remove the box
    ax.xaxis.line.set_lw(0.)
    ax.yaxis.line.set_lw(0.)
    ax.zaxis.line.set_lw(0.)
    # make the box panels transparent
    ax.xaxis.pane.set_edgecolor('w')
    ax.xaxis.pane.set_alpha(0.0)
    ax.yaxis.pane.set_edgecolor('w')
    ax.yaxis.pane.set_alpha(0.0)
    ax.zaxis.pane.set_edgecolor('w')
    ax.zaxis.pane.set_alpha(0.0)

    return fig, ax




if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python generateFrames.py <directory> <fnameInitConds>")
        print("Example: python generateFrames.py ../outputDATA/isotropic-plummer-1000/ initCond-isotropic-Plummer-1000-particles.txt")
        sys.exit(1)
    else:
        directory = sys.argv[1]
        fnameInitConds = sys.argv[2]
        main(directory,fnameInitConds)

