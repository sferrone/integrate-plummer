import numpy as np
import numba as nb
import time 
import os 
import json
import sys 

@nb.njit
def PlummerAccelNumba(x, y, z, M, Rc, G=1):
    """
    Compute the acceleration on each particle due to the Plummer sphere and potential energy
    """
    r = np.sqrt(x**2 + y**2 + z**2)
    r3 = r**3
    ax = -G*M*x/r3*(1 + r**2/(3*Rc**2))
    ay = -G*M*y/r3*(1 + r**2/(3*Rc**2))
    az = -G*M*z/r3*(1 + r**2/(3*Rc**2))
    U = -G*M/(r*(1 + r**2/(2*Rc**2)))
    return ax, ay, az, U


@nb.njit
def leapfrog(x, y, z, vx, vy, vz, ax, ay, az, dt, M, Rc, G=1):
    """
    Leapfrog integrator
    """
    xnew = x + vx*dt + 0.5*ax*dt**2
    ynew = y + vy*dt + 0.5*ay*dt**2
    znew = z + vz*dt + 0.5*az*dt**2
    axnew, aynew, aznew, U = PlummerAccelNumba(xnew, ynew, znew, M, Rc, G)
    vxnew = vx + 0.5*(axnew+ax)*dt
    vynew = vy + 0.5*(aynew+ay)*dt
    vznew = vz + 0.5*(aznew+az)*dt
    return xnew, ynew, znew, vxnew, vynew, vznew, axnew, aynew, aznew, U



def main(fname,fnameParams, numObs=4):
    # import the initial conditions
    # import the parameters
    

    with open(fnameParams, 'r') as fp:
        params = json.load(fp)
    data = np.loadtxt(fname)
    
    x,y,z = data[:,0], data[:,1], data[:,2]
    vx,vy,vz = data[:,3], data[:,4], data[:,5]
    ax,ay,az = np.zeros_like(x), np.zeros_like(y), np.zeros_like(z)
    U = np.zeros_like(x)
    N = len(x)
    tmax = params['period']
    c=0
    ctotal = 4*int(tmax/params['dt'])
    print("ctotal", ctotal)
    # make output directory
    outdir = "../outputDATA/"
    os.makedirs(outdir, exist_ok=True)    
    outdir = "../outputDATA/isotropic-plummer-{:d}/".format(N)
    os.makedirs(outdir, exist_ok=True)
    print("saving to: ", outdir)
    # begin the integration 
    t0 = time.time()
    chunk = int(ctotal/10)
    # do the first acceleration calculation
    ax, ay, az, U = PlummerAccelNumba(x, y, z, params["mass"], params["Rc"],G=params["G"])
    while c < ctotal:
        x, y, z, vx, vy, vz, ax, ay, az, U = leapfrog(x, y, z, vx, vy, vz, ax, ay, az, \
                                                      params['dt'], params['mass'], params['Rc'], G=params['G'])
        c+=1
        if c%100 == 0:
            np.savetxt(outdir+"frame-{:s}.txt".format(str(c).zfill(6)), np.array([x,y,z,vx,vy,vz]).T)

        if c%chunk == 0:
            print("Completed: ", 100*c/ctotal, "%")
    t1 = time.time()
    print("Time taken: ", t1-t0)




def test():
    # make some fake positions and velocities
    Np=10
    x,y,z = np.random.rand(Np), np.random.rand(Np), np.random.rand(Np)
    vx,vy,vz = np.random.rand(Np), np.random.rand(Np), np.random.rand(Np)
    ax,ay,az = np.zeros_like(x), np.zeros_like(y), np.zeros_like(z)
    U = np.zeros_like(x)
    Mt=10
    Rc=2
    dt=0.01
    G=1
    # do the first acceleration calculation
    ax, ay, az, U = PlummerAccelNumba(x, y, z, Mt, Rc,G=G)
    print("ax",ax)



if __name__ == "__main__":
    """
    Integrate the orbits of a set of particles using the leapfrog integrator
    the first argument is the name of the file containing the initial conditions
    the second argument is the name of the file containing the parameters
    """
    fnameInitConds=sys.argv[1]
    fnameParams=sys.argv[2]
    if len(sys.argv) > 3:
        numObs = int(sys.argv[3])
    else:
        numObs = 4
    main(fnameInitConds,fnameParams, numObs=numObs)